
public class StudentHome_html {
	
	public static final String html = "<html>\n" + 
			"<head>\n" + 
			"  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\">\n" + 
			"<style>\n" + 
			"\n" + 
			".topnav {\n" + 
			"  overflow: hidden;\n" + 
			"  top:0;\n" + 
			"  width:100%;\n" + 
			"  position:fixed;\n" + 
			"  background-color: #555;\n" + 
			"}\n" + 
			"\n" + 
			".topnav a {\n" + 
			"  float: left;\n" + 
			"  color: #f2f2f2;\n" + 
			"  text-align: center;\n" + 
			"  padding: 14px 16px;\n" + 
			"  text-decoration: none;\n" + 
			"  font-size: 17px;\n" + 
			"}\n" + 
			"\n" + 
			".topnav a:hover {\n" + 
			"  background-color: #ddd;\n" + 
			"  color: black;\n" + 
			"}\n" + 
			"\n" + 
			".topnav a.active {\n" + 
			"  background-color: #4CAF50;\n" + 
			"  color: white;\n" + 
			"}\n" + 
			"\n" + 
			".courses{\n" + 
			"    width: 50%;\n" + 
			"    margin-top: 15px;\n" + 
			"    height: 50vh;\n" + 
			"    margin-right: auto; \n" + 
			"    margin-left: auto;\n" + 
			"    overflow-y: auto;\n" + 
			"}\n" + 
			"\n" + 
			".course{\n" + 
			"   padding: 5px;\n" + 
			"   background-color: #FAA; \n" + 
			"   margin: 5px;\n" + 
			"   overflow: hidden;\n" + 
			"}\n" + 
			"\n" + 
			".img{\n" + 
			"  margin-left: 15px;\n" + 
			"  height: 100px;\n" + 
			"  width:100px;\n" + 
			"  display: inline-block;  \n" + 
			"}\n" + 
			"\n" + 
			".grid-container {\n" + 
			"  display: grid;\n" + 
			"  width: 100%;\n" + 
			"  grid-template-columns: 20% 60% 20%;\n" + 
			"  grid-gap: 0px;\n" + 
			"}\n" + 
			"\n" + 
			".main {\n" + 
			"  padding-top: 16px;\n" + 
			"  margin-top: 30px;\n" + 
			"}\n" + 
			"\n" + 
			".item1 {\n" + 
			"  grid-row-start: 1;\n" + 
			"  grid-row-end: 2;\n" + 
			"  height: 10vh;\n" + 
			"  background-color: #aaa;\n" + 
			"}\n" + 
			"\n" + 
			".item2 {\n" + 
			"  grid-row-start: 2;\n" + 
			"  grid-row-end: 8;\n" + 
			"  height: 85vh;\n" + 
			"  background-color: #bbb;\n" + 
			"  overflow-y: auto;\n" + 
			"  padding-bottom: 30px;\n" + 
			"}\n" + 
			".item3 {\n" + 
			"  grid-row-start: 2;\n" + 
			"  grid-row-end: 9;\n" + 
			"  height: 85vh;\n" + 
			"  background-color: #aba;\n" + 
			"  overflow-y: auto;\n" + 
			"}\n" + 
			"\n" + 
			".msg{\n" + 
			"    height:10vh;\n" + 
			"    background-color:#baa;\n" + 
			"    overflow: hidden;\n" + 
			"}\n" + 
			"\n" + 
			".item4 {\n" + 
			"  grid-row-start: 8;\n" + 
			"  grid-row-end: 9;\n" + 
			"  height: 10vh;\n" + 
			"  margin: 0px;\n" + 
			"  background-color: #baa;\n" + 
			"}\n" + 
			"\n" + 
			"\n" + 
			".summary{\n" + 
			"   width: 90%;\n" + 
			"    height:5vh;\n" + 
			"    padding: 5px;\n" + 
			"    margin-left: auto;\n" + 
			"    margin-right: auto;\n" + 
			"    background-color: #f8f8f8;\n" + 
			"    resize: none;\n" + 
			"}\n" + 
			"\n" + 
			".question{\n" + 
			"   width: 90%;\n" + 
			"    height:20vh;\n" + 
			"    padding: 5px;\n" + 
			"    margin-left: auto;\n" + 
			"    margin-right: auto;\n" + 
			"    background-color: #f8f8f8;\n" + 
			"    resize: none;\n" + 
			"}\n" + 
			"\n" + 
			".modal {\n" + 
			"    display: none; /* Hidden by default */\n" + 
			"    position: fixed; /* Stay in place */\n" + 
			"    z-index: 1; /* Sit on top */\n" + 
			"    padding-top: 100px; /* Location of the box */\n" + 
			"    left: 0;\n" + 
			"    top: 0;\n" + 
			"    width: 100%; /* Full width */\n" + 
			"    height: 100%; /* Full height */\n" + 
			"    overflow: auto; /* Enable scroll if needed */\n" + 
			"    background-color: rgb(0,0,0); /* Fallback color */\n" + 
			"    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */\n" + 
			"}\n" + 
			"\n" + 
			"/* Modal Content */\n" + 
			".modal-content {\n" + 
			"    background-color: #fefefe;\n" + 
			"    margin: auto;\n" + 
			"    padding: 20px;\n" + 
			"    border: 1px solid #888;\n" + 
			"    width: 40%;\n" + 
			"    height: 40vh;\n" + 
			"}\n" + 
			"\n" + 
			"/* The Close Button */\n" + 
			".close {\n" + 
			"    color: #aaaaaa;\n" + 
			"    margin-left: auto;\n" + 
			"    font-size: 28px;\n" + 
			"    font-weight: bold;\n" + 
			"}\n" + 
			"\n" + 
			".close:hover,\n" + 
			".close:focus {\n" + 
			"    color: #000;\n" + 
			"    text-decoration: none;\n" + 
			"    cursor: pointer;\n" + 
			"}" + 
			"\n" + 
			"</style>\n" + 
			"\n" + 
			"<script type=\"text/javascript\" src=\"http://code.jquery.com/jquery-1.7.1.min.js\"></script>\n" + 
			"\n" + 
			"<script src=\"student_home.js\">\n" + 
			"</script>\n" + 
			"\n" + 
			"\n" + 
			"</head>\n" + 
			"\n" + 
			"<body>\n" + 
			"\n" + 
			"<div id=\"topnav\" class=\"topnav\">\n" + 
			"  <a class=\"active\" href=\"#home\">Home</a>\n" + 
			"  <a style=\"float:right\" href=\"#Logout\">Logout</a>\n" + 
			"</div>\n" + 
			"<div id=\"main\" class=\"main\">\n" +
			"<center>\n" + 
			"  \n" + 
			"    <h1 style=\"margin-top: 35px\">Year: 2018 Semester: 2</h1>\n" + 
			" </center>\n" + 
			"\n" + 
			"<div id=\"courses\" class=\"courses\">\n" + 
			"</div>   \n" + 
			"\n" + 
			"</div>\n" + 
			"\n" + 
			"\n" + 
			"	\n" + 
			"</body>\n" + 
			"" ;

}
